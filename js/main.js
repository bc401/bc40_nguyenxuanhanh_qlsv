var DSSV = "DSSV";
var dssv = [];

var dssvJson = localStorage.getItem(DSSV);
if (dssvJson != null) {
  var svArr = JSON.parse(dssvJson);
  dssv = svArr.map(function(item) {
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matkhau,
      item.toan,
      item.ly,
      item.hoa
      );
  });
  renderDSSV(dssv);
}
function themSinhVien() {
  var sv = layThongTinTuForm();
  dssv.push(sv);
  var dssvJson = JSON.stringify(dssv);
  localStorage.setItem(DSSV, dssvJson);
  renderDSSV(dssv); 
}
function xoaSinhVien(idSv) {
  
  var vitri = timkiem(idSv, dssv);
  if(vitri != -1) {
    dssv.splice(vitri,1);
    renderDSSV(dssv);

  }
}
function suaSinhVien(idSv) {
  var vitri = timkiem(idSv, dssv);
  if(vitri == -1) {
    return;
  }
  var sv = dssv[vitri];
  showthongtinlenForm(sv);
}

function capnhatthontin() {
  var sv = layThongTinTuForm();
  console.log("🚀 ~ file: main.js:50 ~ capnhatthontin ~ sv", sv)
  var vitri = timkiem(sv.ma, dssv);
  if(vitri != -1) {
    dssv[vitri] = sv;
    renderDSSV(dssv);
  }
}



