function SinhVien(_maSv,_tenSv,_email,_matKhau,_diemToan,_diemLy,_diemHoa) {
    this.ma = _maSv;
    this.ten =_tenSv;
    this.email = _email;
    this.matkhau = _matKhau;
    this.toan = _diemToan;
    this.ly = _diemLy;
    this.hoa = _diemHoa;
    this.tinhDTB = function () {
        return Math.floor((this.toan + this.ly + this.hoa) / 3);
    }
};